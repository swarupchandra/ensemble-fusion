import math

base = '/home/swrup/Documents/research/multistream/scale-fusion/'
name = ['fc_fusion', 'fc_scale', 'fc_scalemax',
		'kdd_fusion', 'kdd_scale', 'kdd_scalemax',
		'pamap_fusion', 'pamap_scale', 'pamap_scalemax',
		'syn002_fusion', 'syn002_scale', 'syn002_scalemax',
		'syn003_fusion', 'syn003_scale', 'syn003_scalemax',
		'syned_fusion', 'syned_scale', 'syned_scalemax',
		'elec_fusion', 'elec_scale', 'elec_scalemax']

for f in name:
	content = open(base + 'cpoints/change_points_' + f + '.txt').readlines()
	tpoints = list()
	for l in content:
		split = map(lambda x: x.strip().split('='), l.split('\t'))
		tpoints.append(int(split[1][1]))

	count = 1
	with open(base + 'cpoints_input/cp_'+f+'.csv', 'w') as f2:
		f2.write('i,c\n')
		curr_lim = 10000
		for t in tpoints:
			if t > curr_lim:
				f2.write(str(curr_lim) + ',' + str((count)) + '\n')
				curr_lim += 10000
			count += 1



